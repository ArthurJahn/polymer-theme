/*
Copyright (c) 2015 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var runSequence = require('run-sequence');

var helper = require('./tasks/helpers.js');

var getTask = function(task) {
  var taskPath = './tasks/' + task;
  return require(taskPath)(gulp, $);
};

// Copy theme files at the .theme dir
gulp.task('theme-files', function() {
  return getTask('theme-task').themeFiles();
});

//fix import and links inside index.html
gulp.task('theme-paths', function() {
  return getTask('theme-task').themePaths();
});

// Noosfero theme task
gulp.task('theme', function() {
  runSequence('default', 'theme-paths', 'theme-files');
});

// Compile and automatically prefix stylesheets
gulp.task('styles', function() {
  return getTask('build-task').styleTask('styles', ['**/*.css']);
});

// Compile and automatically prefix stylesheets
gulp.task('elements', function() {
  return getTask('build-task').styleTask('elements', ['**/*.css']);
});

// Ensure that we are not missing required files for the project
// "dot" files are specifically tricky due to them being hidden on
// some systems.
gulp.task('ensureFiles', function() {
  return getTask('build-task').ensureFiles();
});

// Optimize images
gulp.task('images', function() {
  return getTask('build-task').imageOptimizeTask();
});

// Copy all files at the root level (app)
gulp.task('copy', function() {
  return getTask('build-task').copyApp();
});

// Copy web fonts to dist
gulp.task('fonts', function() {
  return getTask('build-task').copyFonts();
});

// Scan your HTML for assets & optimize them
gulp.task('html', function() {
  return getTask('build-task').optimizeHtmlTask();
});

// Vulcanize granular configuration
gulp.task('vulcanize', function() {
  return getTask('build-task').vulcanize();
});

// Clean output directory
gulp.task('clean', function() {
  return getTask('build-task').clean();
});

// Watch files for changes & reload
gulp.task('serve', ['styles', 'elements'], function() {
  return getTask('dev-task').serve();
});

// Build production files
gulp.task('default', ['clean'], function(cb) {
  runSequence(
    ['ensureFiles', 'copy', 'styles'],
    'elements',
    ['images', 'fonts', 'html'],
    'vulcanize',
    cb);
});

// Load tasks for web-component-tester
// Adds tasks for `gulp test:local` and `gulp test:remote`
require('web-component-tester').gulp.init(gulp);
