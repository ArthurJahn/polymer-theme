var path = require('path');

var ret = {
  'suites': ['app/test'],
  'webserver': {
    'pathMappings': []
  },
  'verbose': false,
  'plugins': {
    'local': {
      'browsers': ['chrome', 'firefox']
    },
    'sauce': {
      'disabled': true,
      'browsers': [{
          'browserName': 'microsoftedge',
          'platform': 'Windows 10',
          'version': ''
        }, {
          'browserName': 'internet explorer',
          'platform': 'Windows 8.1',
          'version': '11'
        },
        {
          'browserName': 'safari',
          'platform': 'OS X 10.11',
          'version': '9'
        }
      ]
    },
    'istanbul': {
      dir: './coverage',
      reporters: ['text-summary', 'lcov'],
      include: [
        '/app/elements/**/*.html',
        '/app/elements/*.html',
        '/app/index.html'
      ],
      exclude: [
      ],
      thresholds: {
        global: {
          statements: 90
        }
      }
    }
  }
};

var mapping = {};
var rootPath = (__dirname).split(path.sep).slice(-1)[0];

mapping['/components/' + rootPath  +
'/app/bower_components'] = 'bower_components';

ret.webserver.pathMappings.push(mapping);

module.exports = ret;
