var helper = require('./helpers');
var path = require('path');
var merge = require('merge-stream');
var del = require('del');
var fs = require('fs');


module.exports = function (gulp, $) {
  var modules = {
    // Minify css and send it to dest folder
    styleTask: function(stylesPath, srcs) {
      return gulp.src(srcs.map(function(src) {
          return path.join('app', stylesPath, src);
        }))
        .pipe($.changed(stylesPath, {extension: '.css'}))
        .pipe($.autoprefixer(helper.prefixerBrowsers()))
        .pipe(gulp.dest('.tmp/' + stylesPath))
        .pipe($.minifyCss())
        .pipe(gulp.dest(helper.dist(stylesPath)))
        .pipe($.size({title: stylesPath}));
    },

    // Optimize images
    imageOptimizeTask: function() {
      var src = 'app/images/**/*';
      var dest = helper.dist('images');
      return gulp.src(src)
        .pipe($.imagemin({
          progressive: true,
          interlaced: true
        }))
        .pipe(gulp.dest(dest))
        .pipe($.size({title: 'images'}));
    },

    copyApp: function() {
      var app = gulp.src([
        'app/*',
        '!app/test',
        '!app/elements',
        '!app/bower_components',
        '!app/cache-config.json',
        '!**/.DS_Store'
      ], {
        dot: true
      }).pipe(gulp.dest(helper.dist()));

      // Copy over only the bower_components we need
      // These are things which cannot be vulcanized
      var bower = gulp.src([
        'app/bower_components/{webcomponentsjs,promise-polyfill}/**/*'
      ]).pipe(gulp.dest(helper.dist('bower_components')));

      return merge(app, bower)
        .pipe($.size({
          title: 'copy'
        }));
    },

    optimizeHtmlTask: function() {
      var src = [
        'app/**/*.html',
        '!app/{elements,test,bower_components}/**/*.html'
      ];
      var dest = helper.dist();
      var assets = $.useref.assets({
        searchPath: ['.tmp', 'app']
      });

      return gulp.src(src)
        .pipe(assets)
        // Concatenate and minify JavaScript
        .pipe($.if('*.js', $.uglify({
          preserveComments: 'some'
        })))
        // Concatenate and minify styles
        // In case you are still using useref build blocks
        .pipe($.if('*.css', $.minifyCss()))
        .pipe(assets.restore())
        .pipe($.useref())
        // Minify any HTML
        .pipe($.if('*.html', $.minifyHtml({
          quotes: true,
          empty: true,
          spare: true
        })))
        // Output files
        .pipe(gulp.dest(dest))
        .pipe($.size({
          title: 'html'
        }));
    },

    copyFonts: function() {
      return gulp.src(['app/fonts/**'])
        .pipe(gulp.dest(helper.dist('fonts')))
        .pipe($.size({
          title: 'fonts'
        }));
    },

    vulcanize: function() {
      return gulp.src('app/elements/elements.html')
        .pipe($.vulcanize({
          stripComments: true,
          inlineCss: true,
          inlineScripts: true
        }))
        .pipe(gulp.dest(helper.dist('elements')))
        .pipe($.size({title: 'vulcanize'}));
    },

    clean: function() {
      return del(['.tmp', helper.dist('**')]);
    },

    ensureFiles: function() {

      var files = [path.join(__dirname, '../.bowerrc')];
      var missingFiles = files.reduce(function(prev, filePath) {
        var fileFound = false;
        try {
          fileFound = fs.statSync(filePath).isFile();
        } catch (e) { }

        if (!fileFound) {
          prev.push(filePath + ' Not Found');
        }
        return prev;
      }, []);

      if (missingFiles.length) {
        var err = new Error('Missing Required Files\n' + missingFiles.join('\n'));
        throw err;
      }
    }
  };

  return modules;
}
