var helper = require('./helpers.js');

module.exports = function (gulp, $) {
  var modules = {
    //fix import and links inside index.html
    themePaths: function () {
      gulp.src(helper.dist('index.html'))
        .pipe($.replace('elements/elements.html', helper.themePath() + 'elements/elements.html'))
        .pipe($.replace('styles/main.css', helper.themePath() + 'styles/main.css'))
        .pipe($.replace('bower_components', helper.themePath() + 'bower_components'))
        .pipe($.replace('scripts/app.js', helper.themePath() + 'scripts/app.js'))
        .pipe($.rename('index.html.erb'))
        .pipe(gulp.dest(helper.dist()))
        .pipe($.size({title: 'theme-paths'}));
    },

    // Copy theme files at the .theme dir
    themeFiles: function () {
      gulp.src([
        '.theme/**',
        '!**/.DS_Store'
      ], {dot: true})
      .pipe(gulp.dest(helper.dist()))
      .pipe($.size({title: 'theme-files'}));
    }
  };

  return modules;
};
