var path = require('path');
var fs = require('fs');

var DIST = './dist';
var THEME_PREFIX = 'designs/themes';
var AUTOPREFIXER_BROWSERS = [
  'ie >= 10',
  'ie_mob >= 10',
  'ff >= 30',
  'chrome >= 34',
  'safari >= 7',
  'opera >= 23',
  'ios >= 7',
  'android >= 4.4',
  'bb >= 10'
];

module.exports = {

  dist: function(subpath) {
    return !subpath ? DIST : path.join(DIST, subpath);
  },

  themePath: function() {
    var themeFile = fs.readFileSync('.theme/theme.yml', 'utf-8');
    var name = themeFile.match(/name: "(.*)"/)[1].replace(' ', '_').
      toLowerCase() || 'polymer_default';
    return path.join(THEME_PREFIX, name,'/');
  },

  prefixerBrowsers: function() {
    return AUTOPREFIXER_BROWSERS;
  }

};
